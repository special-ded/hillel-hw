
let itemsArr=[
  { 
         name: 'pen',
         price: 2,
         category: 'pens'
       },

    { 
           name: 'pencil',
           price: 1,
           category: 'pencil'
         },
         { 
          name: 'red pencil',
          price: 1,
          category: 'pencil'
        }
];

// Напишите функцию, которая принимает название товара, его цену и категорию,
//  добавляет этот товар в массив, где каждый товар это объект и 
//  возвращает массив всех товаров. Товаров может быть сколько угодно.
function addItem(itemName, itemPrice, itemCategory,arr){
  let name,price,category;
    name = itemName,
    price = itemPrice,
    category = itemCategory

  arr.push({name, price, category})
  return{
    name,
    price,
    category
  }
}
addItem('eraser', 55, 'erasers',itemsArr)
console.log(itemsArr)


// - Напишите функцию, которая фильтрует товары по цене от и до 
// и возращает новый массив только с товарами выбранного ценового диапазона
//  или пустой массив.
const priceFromAndTo = (from, to, arr) => arr.filter(el => el.price >= from && el.price <= to);
console.log('priceFromAndTo ')
console.log(priceFromAndTo(2, 56, itemsArr));

// - Напишите функцию, которая фильтрует товары по категории и возращает
//  новый массив только с товарами выбранной категории, если она есть или
//   пустой массив.
const getCategory = (categoryName, arr) => arr.filter(el => el.category == categoryName);
console.log('Filtering items by category-  ')
console.log( getCategory('pencil', itemsArr));


// - Напишите функцию, которая возвращает количесто товаров в категории.
console.log('Quantity of items in category-  ' + getCategory.length)


// - Напишите функцию, которая удаляет товар по имени.
const deleteItem = (itemName, arr) =>{
  for(let i=0; i< arr.length; i++){
      
      if(arr[i].name == itemName){
         return arr.splice(i, 1);
      }
      
  }
  return false;
}
deleteItem('red pencil',itemsArr );
console.log('Deleted - red pencil');
console.log(itemsArr);


// - Напишите функции, которые сортируют товары по цене от меньшего к 
// большему и наоборот и возвращают новый массив.
console.log('Sort - Increasing Price');
let sortIncreasingPrice = (a,b) => a.price>b.price ? 1 : -1;
itemsArr.sort(sortIncreasingPrice);
console.log(itemsArr);

const Arr2 = itemsArr.map(a => ({...a}));
console.log('Sort - Decreasing Price');
let sortDecreasingPrice  = (a,b) => b.price>a.price ? 1 : -1;
Arr2.sort(sortDecreasingPrice);
console.log(Arr2);



// - Напишите функцию, котрая принимает вид сортировки (от большего к 
//   меньшему или наоборот) и фильтра (диапазон цены или категория) и 
//   возвращает новый массив товаров определённой выборки, отсортированные
//    как указал пользователь.
const sortByPrice = (arr, sortingMark) => arr.slice('').sort((a, b) => (Number(a.price) > 
Number(b.price)) ? 1 * sortingMark : ((Number(a.price) < Number(b.price)) ? -1 * sortingMark : 0));

const selectAndSort = (arr, sortingMark, ...rest) => {
  let buff;
  if(rest.length == 2)
      buff = priceFromAndTo(rest[0], rest[1], arr);
  else if(rest.length == 1)
      buff = getCategory(rest[0], arr);
      return sortByPrice(buff, sortingMark);
}
console.log(selectAndSort(itemsArr, 1, 1, 900));
console.log(selectAndSort(itemsArr, 1, 'pencil'));



// - Напишите функцию, котрая принимает фильтра (диапазон цены или категория)
//  и возвращает сумму цен товаров этой выборки.
console.log('Summ of prices')
const selectAndSum = (arr, ...rest) => {
  let buff;
  if(rest.length == 2)
      buff = priceFromAndTo(rest[0], rest[1], arr);
  else if(rest.length == 1)
      buff = getCategory(rest[0], arr);
  return buff.reduce((sum, el) => sum + el.price, 0);
}
console.log(selectAndSum(itemsArr, 1, 200));
console.log(selectAndSum(itemsArr, 'pens'));
